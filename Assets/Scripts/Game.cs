using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLivesCount = 3;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private TextMeshProUGUI finalCoinsCount;
    [SerializeField] private string firstLevelName = "Level1";
    private int livesCount;
    private int coinsCount;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        livesCount = startLivesCount;
        coinsCount = 0;
        ShowLives();
        ShowCoins();
        SceneManager.LoadScene(firstLevelName);
        mainMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoseLive()
    {
        livesCount--;
        if (livesCount <= 0)
        {
            livesCount = 0;
            GameOver();
            ShowLives();
    }
    
    }
    private void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        finalCoinsCount.text = coinsCount.ToString();
    }

    public void RestartGame()
    {
        livesCount = startLivesCount;
        coinsCount = 0;
        ShowLives();
        ShowCoins();
    }
    public void AddCoin(int amount)
    {
        coinsCount += amount;
        ShowCoins();
    }
    private void ShowLives()
    {
        livesText.text = livesCount.ToString();
    }

    private void ShowCoins()
    {
        coinsText.text = coinsCount.ToString();
    }
}
