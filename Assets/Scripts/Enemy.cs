using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    [SerializeField] private string groundLayer = "Ground";
    private Rigidbody2D enemiRigidbody;
    private bool isFacingRight;

    private void Awake()
    {
        enemiRigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector2 velocity = new Vector2(isFacingRight ? speed : -speed, enemiRigidbody.velocity.y);
        enemiRigidbody.velocity = velocity;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(groundLayer)) return;

        Flip();
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}   


 